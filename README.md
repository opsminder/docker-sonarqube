# docker-sonarqube
Provide an openjdk, ubuntu-based docker image for sonarqube.

# overview

The `docker-compose.yml` file here will build the sonarqube container locally, and will start up that container plus a postgresql container
and an adminer container (for troubleshooting).  The sonarqube container
will be available at http://localhost:9000 and adminer will be at http://localhost:8087.  Login to sonarqube with admin/admin.  For adminer, change the database type to postgresql and login as sonarqube/sonarqube to database
sonarqube.



## Background

This project was created in mid-2021 to address a couple of issues with the
existing dockerfile for sonarqube running on a local M1 mac.  The initial motive was that the official dockerfile (and image) wouldn't run deterministically on a Mac M1 under docker (the ARM version of docker).  When running the raw image, we saw various outputs / results and could not get consistent output.  We variously saw:
- no output at all from the sonarqube image
- JDK stack traces relating to file parsing or other errors
- internal JDK-related core dumps

Our non-M1 colleagues saw no issues.

Cleaning between runs involved cleaning out the docker host via:
```
docker compose down
docker system prune -fa --volumes
```

We tried various solutions:
- using --platform=linux/amd64 for the alpine sonarqube image
- starting under rosetta

