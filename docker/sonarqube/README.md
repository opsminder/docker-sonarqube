# sonarqube dockerqube
This directory contains two dockerfiles to build sonarqube.  One, `sonarqube.dockerfile` is the
original dockerfile for Sonarqube version 8.9.1 community edition.  This dockerfile uses alpine
with the openjdk.  Openjdk does not support alpine.  When building and running this dockerfile
from an M1 mac, the running container is non-deterministic.  On clean start, it sometimes throws java exceptions,
sometimes produces no output, and sometimes SEGV's.  To fix these errors, we ported
the installation to the official openjdk container, which uses ubuntu as a base.  This
port is in the second dockerfile, `fromjdk.dockerfile`.  This second file will properly build
an ubuntu-based sonarqube server on an M1 (as ARM64) and runs without the errors noted above.

